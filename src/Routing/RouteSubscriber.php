<?php

namespace Drupal\site_payments_raiffeisenbank\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\Core\State\StateInterface;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(StateInterface $state) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Change path '/api/v1/webhook/raiffeisenbank' to '/webhook/{id}'.
    if ($route = $collection->get('rest.site_payments_raiffeisenbank_webhook.POST')) {
      $id = (string) $this->state->get('site_payments_raiffeisenbank.webhook_id');
      if ($id) {
        $route->setPath("/webhook/{$id}");
      }
    }
  }

}
