<?php

namespace Drupal\site_payments_raiffeisenbank\Plugin\rest\resource\v1;

use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\ResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource for rest: webhook for Raiffeisenbank.
 *
 * @RestResource(
 *   id = "site_payments_raiffeisenbank_webhook",
 *   label = @Translation("SITE COMMERCE: webhook for Raiffeisenbank"),
 *   uri_paths = {
 *     "create" = "/api/v1/webhook/raiffeisenbank",
 *   }
 * )
 */
class Webhook extends ResourceBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    return $instance;
  }

  /**
   * Responds to POST requests.
   */
  public function post(array $data) {
    $response['result'] = NULL;

    try {
      kvantstudio_debug('site_payments', 'Webhook for Raiffeisenbank', $data);

      return new ModifiedResourceResponse($response);
    } catch (\Exception $e) {
      return new ResourceResponse('Something went wrong. Check your data.', 400);
    }
  }

}
