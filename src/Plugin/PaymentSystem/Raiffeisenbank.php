<?php

namespace Drupal\site_payments_raiffeisenbank\Plugin\PaymentSystem;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use Drupal\kvantstudio\Validator;
use Drupal\site_payments\Event\PaymentStatusEvent;
use Drupal\site_payments\PaymentSystemPluginBase;
use Drupal\site_payments\ReceiptInterface;
use Drupal\site_payments\TransactionInterface;
use GuzzleHttp\Client as Guzzle;
use Raiffeisen\Ecom\Client;

/**
 * @PaymentSystem(
 *   id="site_payments_raiffeisenbank",
 *   label = @Translation("Raiffeisenbank"),
 *   payment_method_name = @Translation("Payment by card online (Raiffeisenbank)")
 * )
 */
final class Raiffeisenbank extends PaymentSystemPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getPaymentSettings(?int $payment_account_id = NULL): array {
    $values = \Drupal::state()->getMultiple([
      'site_payments.allow_test_payment',
      'site_payments.test_hostnames',
      'site_payments.test_usernames',
      'site_payments_raiffeisenbank.block_rules_payment_system'
    ]);

    $array_state = [
      'allow_test_payment' => (bool) ($values['site_payments.allow_test_payment'] ?? FALSE),
      'test_hostnames' => $values['site_payments.test_hostnames'] ?? NULL,
      'test_usernames' => $values['site_payments.test_usernames'] ?? NULL,
      'block_rules_payment_system' => (int) ($values['site_payments_raiffeisenbank.block_rules_payment_system'] ?? 0),
    ];

    $array_account = $this->getAccount($payment_account_id);
    if ($array_account) {
      return array_merge($array_state, $array_account);
    }

    return $array_state;
  }

  /**
   * {@inheritdoc}
   */
  public function getAvailableStatus(): bool {
    $settings = $this->getPaymentSettings();
    return (bool) ($settings['status'] ?? FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getRulesPaymentSystem(): array {
    $settings = $this->getPaymentSettings();

    $build = [];
    if ($settings['block_rules_payment_system']) {
      $entity_type = 'block_content';
      $view_mode = 'default';
      $block = $this->entityTypeManager->getStorage($entity_type)->load($settings['block_rules_payment_system']);
      $view_builder = $this->entityTypeManager->getViewBuilder($entity_type);
      $build = $view_builder->view($block, $view_mode);
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getWebhookUrl(): string {
    $route_name = 'site_payments_raiffeisenbank.payment_webhook';
    $url = Url::fromRoute($route_name);

    return $url->setAbsolute()->toString();
  }

  /**
   * {@inheritdoc}
   */
  public function getOrderId(TransactionInterface $transaction): int|string {
    return $transaction->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentUrl(array $payment_data): string {
    // Link for payment URL page.
    $payment_url = '';

    // We determine which bank account to choose in the payment system.
    $payment_account_id = empty($payment_data['payment_account']) ? NULL : (int) $payment_data['payment_account'];

    // Payment system settings for a specific bank account.
    $config = $this->getPaymentSettings($payment_account_id);
    $payment_data['payment_account'] = $config['id'];

    // Expiration time of the payment link.
    $overdue = (int) empty($config['overdue']) ? 28800 : ($config['overdue'] * 3600);
    $request_time = \Drupal::time()->getRequestTime();
    $payment_data['overdue'] = $request_time + $overdue;

    // The name of the payment plugin.
    if (empty($payment_data['payment_plugin'])) {
      $payment_data['payment_plugin'] = $this->getId();
    }

    // The name of the payment system.
    if (empty($payment_data['payment_system'])) {
      $payment_data['payment_system'] = $config['payment_system'];
    }

    // The cost must be rounded to two decimal places after the separator.
    $payment_data['price'] = round($payment_data['price'], 2);

    // Register a transaction.
    $transaction = $this->createTransaction($payment_data, TRUE);
    if (!$transaction) {
      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Error before generate payment URL. Transaction not created: {$error_data}.");
      return $payment_url;
    }

    // Check if the cost exists, that it is greater than zero.
    $payment = [];
    if (Validator::comparingNumbers($payment_data['price'])) {
      // Change the price for transaction.
      $payment_data['price'] = $this->correctPaidNumber($transaction, $config);

      // Register receipt.
      $receipt = $this->createReceipt($transaction);

      // Generate a unique order number order_id -a sequence of Latin letters, numbers and symbols with a length not exceeding 32 characters.
      $order_id = $this->getOrderId($transaction);

      $params = [
        'order_id' => $order_id,
        'price' => $payment_data['price'],
        'success_redirect' => empty($payment_data['success_redirect_route']) ? $this->getSuccessUrl($transaction->uuid()) : $this->getSuccessUrl($transaction->uuid(), $payment_data['success_redirect_route']),
        'failure_redirect' => empty($payment_data['failure_redirect_route']) ? $this->getFailureUrl($transaction->uuid()) : $this->getFailureUrl($transaction->uuid(), $payment_data['failure_redirect_route']),
        'overdue' => $payment_data['overdue'],
        'token' => $config['token'],
        'merchant' => $config['merchant'],
        'mode' => $config['mode'],
        'verify_ssl' => $config['verify_ssl'],
        'receipt' => $receipt->getData()
      ];
      $payment = $this->getPaymentFormUrl($params);
    }

    // Get a link to pay from the result of the request.
    if ($payment['link']) {
      $payment_url = $payment['link'];

      // Assign the data received from the payment system to the transaction.
      $transaction->setPaymentLink($payment['link']);
      $transaction->setPaymentStatus('issued');
      $transaction->save();
    } else {
      $error_data = json_encode($payment_data, JSON_UNESCAPED_UNICODE);
      $this->logger->error("Error while generate payment URL: {$error_data}.");
    }

    return $payment_url;
  }

  /**
   * Creates an order in the payment system.
   * TODO: метод не регистрирует заказ в банке - это только форма для вывода в js. Техподдержка внятного ответа пока не дала.
   * Поэтому используем просто вызов $this->getPaymentFormUrl($data).
   *
   * @param array $data
   * @return array|null
   */
  public function registerOrder(array $data): ?array {
    $result['link'] = NULL;

    try {
      $host = $data['mode'] ? Client::HOST_PROD : Client::HOST_TEST;
      $client = new Client($data['token'], $data['merchant'], $host);

      // An order identifier.
      $order_id = $data['order_id'];

      // An order amount.
      $amount = $data['price'];

      // Additional data.
      $params['successUrl'] = $data['success_redirect'];
      $params['failUrl'] = $data['failure_redirect'];
      if ($data['overdue']) {
        $params['expirationDate'] = date('c', $data['overdue']);
      }
      $params['receipt'] = $data['receipt'];

      /** @var \Raiffeisen\Ecom\Client $client */
      $response = $client->postPayUrl($amount, $order_id, $params);

      // Логируем данные.
      $debug = var_export($response, true);
      $this->logger->debug($debug);

      $response = $client->getOrder($order_id);
      $response = $client->getOrderReceipts($order_id);

      $result = $this->getPaymentFormUrl($data);
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }

  /**
   * Get payment form url.
   *
   * @param array $data
   * @return array|null
   */
  private function getPaymentFormUrl(array $data): ?array {
    $result['link'] = NULL;

    try {
      $host = $data['mode'] ? Client::HOST_PROD : Client::HOST_TEST;
      $client = new Client($data['token'], $data['merchant'], $host);

      // An order identifier.
      $order_id = $data['order_id'];

      // An order amount.
      $amount = $data['price'];

      // Additional data.
      $params['successUrl'] = $data['success_redirect'];
      $params['failUrl'] = $data['failure_redirect'];
      if ($data['overdue']) {
        $params['expirationDate'] = date('c', $data['overdue']);
      }

      /** @var \Raiffeisen\Ecom\Client $client */
      $result['link'] = $client->getPayUrl($amount, $order_id, $params);
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function checkPaymentStatus(TransactionInterface $transaction, array $settings = []): ?string {
    $result = NULL;

    try {
      // Settings for payment system.
      $payment_account = $transaction->getPaymentAccountId();
      $config = array_merge($this->getPaymentSettings($payment_account), $settings);

      // Create params for query to payment system.
      $params = [
        'order_id' => $this->getOrderId($transaction),
        'status' => $transaction->getPaymentStatus(),
        'result' => NULL
      ];

      // Dispatch event CHECK_PAYMENT_STATUS_BEFORE.
      $event = new PaymentStatusEvent($transaction, $params);
      $this->eventDispatcher->dispatch($event, PaymentStatusEvent::CHECK_PAYMENT_STATUS_BEFORE);

      // Reload params.
      $transaction = $event->getTransaction();
      $params = $event->getData();

      $host = $config['mode'] ? Client::HOST_PROD : Client::HOST_TEST;
      $client = new Client($config['token'], $config['merchant'], $host);
      $response = $client->getOrderTransaction($params['order_id']);
      $params['result'] = $response;

      if (isset($response['transaction']['id'])) {
        $transaction->setPaymentId($response['transaction']['id'])->save();
      }

      if (isset($response['transaction']['status']['value']) && $response['transaction']['status']['value'] == 'SUCCESS') {
        $price = (float) $transaction->getPrice(TRUE);
        $amount = (float) $response['transaction']['amount'];
        if (Validator::comparingNumbers($price, $amount, FALSE, 2) === 0) {
          $result = 'paid';
          $params['status'] = $result;

          // Dispatch event PAYMENT_STATUS_PAID.
          $event = new PaymentStatusEvent($transaction, $params);
          $this->eventDispatcher->dispatch($event, PaymentStatusEvent::PAYMENT_STATUS_PAID);
          $transaction = $event->getTransaction();
          $params = $event->getData();
        }
      }

      // Dispatch event CHECK_PAYMENT_STATUS_AFTER.
      $event = new PaymentStatusEvent($transaction, $params);
      $this->eventDispatcher->dispatch($event, PaymentStatusEvent::CHECK_PAYMENT_STATUS_AFTER);
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getVatType(?string $type, string $default_type = 'NONE'): int|string {
    if (empty($type)) {
      return $default_type;
    }

    $values = [
      'NONE' => 'NONE',
      '0' => 'VAT0',
      '10' => 'VAT10',
      '110' => 'VAT110',
      '20' => 'VAT20',
      '120' => 'VAT120',
      '5' => 'VAT5',
      '105' => 'VAT105',
      '7' => 'VAT7',
      '107' => 'VAT107'
    ];

    return $values[$type] ?? 'NONE';
  }

  /**
   * {@inheritdoc}
   */
  public function sendReceipt(ReceiptInterface $receipt): ?array {
    $result = NULL;

    try {
      $transaction = $receipt->getTransaction();

      // Data of receipt.
      $data = $receipt->getData();

      // If the receipt is not registered in payment system.
      $result = $this->checkReceiptStatus($receipt);
      if (!$result) {
        $payment_account = $transaction->getPaymentAccountId();
        $config = array_merge($this->getPaymentSettings($payment_account));

        $host = $config['mode'] ? Client::HOST_PROD : Client::HOST_TEST;
        $fiscal_api_url = Client::FISCAL_API_URI;
        $client = new Guzzle();

        // Register receipt in payment system.
        $url = "{$host}{$fiscal_api_url}/receipts/sell";
        $request = $client->post($url, [
          'body' => Json::encode($data),
          'headers' => [
            'Content-Type' => 'application/json;charset=UTF-8',
            'Authorization' => 'Bearer ' . $config['token']
          ],
        ]);
        $content = $request->getBody()->getContents();
        $result = $content ? Json::decode($content) : NULL;
      }

      // Registration of the receipt in OFD.
      if (isset($result['status']) && $result['status'] == 'NEW') {
        $receipt->setStatus($result['status'])->save();

        $url = "{$host}{$fiscal_api_url}/receipts/sell/{$data['receiptNumber']}";
        $request = $client->put($url, [
          'headers' => [
            'Content-Type' => 'application/json;charset=UTF-8',
            'Authorization' => 'Bearer ' . $config['token']
          ],
        ]);
        $content = $request->getBody()->getContents();
        $result = $content ? Json::decode($content) : NULL;

        if (isset($result['status'])) {
          $receipt->setStatus($result['status'])->save();
        }
      } else {
        $receipt->setStatus('FAILED SEND')->save();
      }
    } catch (\Exception $e) {
      Error::logException($this->logger, $e);
    }

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function checkReceiptStatus(ReceiptInterface $receipt): ?array {
    $result = NULL;

    try {
      $transaction = $receipt->getTransaction();

      // Data of receipt.
      $data = $receipt->getData();

      // Настройки платежной системы.
      $payment_account = $transaction->getPaymentAccountId();
      $config = array_merge($this->getPaymentSettings($payment_account));

      $host = $config['mode'] ? Client::HOST_PROD : Client::HOST_TEST;
      $fiscal_api_url = Client::FISCAL_API_URI;
      $client = new Guzzle();

      $url = "{$host}{$fiscal_api_url}/receipts/sell/{$data['receiptNumber']}";
      $request = $client->get($url, [
        'headers' => [
          'Content-Type' => 'application/json;charset=UTF-8',
          'Authorization' => 'Bearer ' . $config['token']
        ],
      ]);
      $content = $request->getBody()->getContents();
      $result = $content ? Json::decode($content) : NULL;

      if (isset($result['status'])) {
        $receipt->setStatus($result['status'])->save();
      }
    } catch (\Exception $e) {
      $code = (int) $e->getCode();
      if ($code != 404) {
        Error::logException($this->logger, $e);
      }
    }

    return $result;
  }
}
