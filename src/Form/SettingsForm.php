<?php

namespace Drupal\site_payments_raiffeisenbank\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\State\StateInterface;

/**
 * Class Form of Raiffeisenbank settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The Sate API object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StateInterface $state) {
    parent::__construct($config_factory);
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state')
    );
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'site_payments_raiffeisenbank_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'site_payments_raiffeisenbank.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $block_rules_payment_system = (int) $this->state->get('site_payments_raiffeisenbank.block_rules_payment_system');
    $block = \Drupal::entityTypeManager()->getStorage('block_content')->load($block_rules_payment_system);
    $form['block_rules_payment_system'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Rules for the use of the payment system'),
      '#target_type' => 'block_content',
      '#default_value' => $block,
      '#description' => $this->t('You need to create a custom block and select it in this field. The block content is displayed under the select payment method field.')
    ];

    $form['webhook_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('ID for the Webhook URL'),
      '#default_value' => $this->state->get('site_payments_raiffeisenbank.webhook_id'),
      '#description' => $this->t("Line. For example, Ct9Yaw5HHu5Dcv0Agd9CDf4CLer7LG."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $value = $form_state->getValue('block_rules_payment_system');
    $this->state->set('site_payments_raiffeisenbank.block_rules_payment_system', $value);

    $value = trim($form_state->getValue('webhook_id', ''));
    $this->state->set('site_payments_raiffeisenbank.webhook_id', $value);
  }
}
